2d-heat-ded
===========

Overview
--------

2d-heat-ded.py implements a two-dimensional (2d) thermal finite element model of the Directed Energy Deposition (DED) process using the Python-based FEniCS framework. The model incrementally deposits material ahead of the laser focus point according to the geometry of the part including three large circular voids. The laser heat energy is supplied by a Gaussian-distributed heat source while the phase change is represented by increased heat capacity around the solidus-liquidus temperature range. Numerical model is calibrated by matching results of the melt pool temperature measurements taken by a dual wave length pyrometer during the build process of a box-shaped Ti-6Al-4V part with large spherical voids.


Files in the package
--------------------

2d-heat-ded.py ... main program, generates/read-in mesh and simulates laser deposition

conf_proc.py ... configuration file for deposition process

conf_geom.py ... configuration file for geometry of a part

conf_mat.py ... configuration file for material properties

mesh.xml ... mesh generated for geometry in conf_geom.py, can be re-used in subsequent runs

Requirements
------------

FEniCS framework, 2d-heat-ded was tested with versions 2017.1.0 and 2019.1.0

Execution
---------

Generate mesh and simulate case 1

  python3 2d-heat-ded.py

Add mesh generated above as command line parameter, then simulate case 1

  python3 2d-heat-ded.py output/mesh.xml

Input parameters
----------------

2d-heat-ded can out-of-box simulate three cases, by changing case in "conf_proc.py"

  case = 1   deposits with left-to-right direction
  
  case = 2   deposits with alternating directions
  
  case = 3   deposits with alternating directions, lower laser power, higher initial temperature

Authors
-------

Bohumir Jelinek <bj48@cavs.msstate.edu>

License
-------

LGPL-3.0

Acknowledgment
--------------

Research was sponsored by the Army Research Laboratory and was
accomplished under Cooperative Agreement Number W911NF-15-2-0025.
Cleared for "Public Release, Distribution Unlimited".
